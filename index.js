const express = require("express");

const PORT = 8080
const app = express()

const fs = require("fs");
const path = require('path');
const morgan = require('morgan');

const jsonParser = express.json();

app.use(morgan("dev"));

const listOfExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']

const filePath = "api/files";

function createdDate(file) {
    const {birthtime} = fs.statSync(file)

    return birthtime
}

app.get("/api/files", function (req, res) {
    if (!req.query) {
        return res.status(400).json({message: 'Client error'})
    }
    try {
        const content = fs.readdirSync(filePath, "utf8");

        res.status(200).json({message: 'Success', files: content});
    } catch (e) {

        res.status(500).json({message: 'Server error'});
    }

});

app.get("/api/files/:filename", function (req, res) {
    const filename = req.params.filename;

    try {
        try {
            const content = fs.readFileSync(`${filePath}/${filename}`, "utf8");

            return res.status(200).json({
                message: 'Success',
                filename: filename,
                content: content,
                extension: filename.split('.').pop(),
                uploadedDate: createdDate(`${filePath}/${filename}`)
            });

        } catch (e) {
            return res.status(400).json({message: `No file with '${filename}' filename found`});
        }

    } catch (e) {
        res.status(500).json({message: 'Server error'});

    }
});

app.post("/api/files", jsonParser, function (req, res) {
    try {
        const fileName = req.body.filename;
        const content = fs.readdirSync(filePath, "utf8");

        if (content.includes(fileName))
            return res.status(400).json({message: 'The file name already exists'});

        const fileExt = path.parse(fileName).ext

        if (!listOfExtensions.includes(fileExt))
            return res.status(400).json({message: 'Incorrect file extension'});

        const fileContent = req.body.content;

        fs.writeFile(`${filePath}/${fileName}`, fileContent, (err) => {
            if (err) {
                return res.status(400).json({message: 'Please specify \'content\' parameter'});
            } else {
                return res.status(200).json({message: "File created successfully"});
            }
        })


    } catch (e) {
        return res.status(500).json({message: 'Server error'});
    }
});


app.delete("/api/files/:filename", function (req, res) {
    try {
        const filename = req.params.filename;

        fs.unlink(`${filePath}/${filename}`, err => {
            if (err) {
                return res.status(400).json({message: 'Incorrect filename'});
            } else {
                return res.status(200).json({message: "File deleted successfully"});
            }
        });
    } catch (e) {
        return res.status(500).json({message: 'Server error'});
    }
});

app.put("/api/files", jsonParser, function (req, res) {

    try {
        const fileName = req.body.filename;
        const content = fs.readdirSync(filePath, "utf8");

        const fileExt = path.parse(fileName).ext

        if (!listOfExtensions.includes(fileExt))
            return res.status(400).json({message: 'Incorrect file extension'});

        const fileContent = req.body.content;

        if (content.includes(fileName))
            fs.writeFile(`${filePath}/${fileName}`, fileContent, (err) => {
                if (err) {
                    return res.status(400).json({message: 'Please specify \'content\' parameter'});
                } else {
                    return res.status(200).json({message: "File updated successfully"});
                }
            })
    } catch (e) {
        return res.status(500).json({message: 'Server error'});
    }
});

app.listen(PORT, function () {
    console.log(`Server has been started on port ${PORT}...`);
});
